package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing volume set on given device that supports SpotifyConnect feature.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Volume
{
	private final int volumePercent;

	@JsonCreator
	public Volume(@JsonProperty("volumePercent") final int volumePercent)
	{
		this.volumePercent = volumePercent;
	}

	public int getVolumePercent()
	{
		return volumePercent;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(volumePercent)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final Volume other = (Volume) obj;
		return new EqualsBuilder()//
				.append(volumePercent, other.volumePercent)//
				.build();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("volumePercent", volumePercent)//
				.build();
	}
}
