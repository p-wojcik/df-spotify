package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing single playlist in Spotify.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Playlist
{
	private final String id;
	private final String name;
	private final String description;
	private final boolean isPublic;
	private final Owner owner;

	@JsonCreator
	public Playlist(//
			@JsonProperty("id") final String id,//
			@JsonProperty("name") final String name,//
			@JsonProperty("description") final String description,//
			@JsonProperty("public") final Boolean isPublic,//
			@JsonProperty("owner") final Owner owner)
	{
		this.id = id;
		this.name = name;
		this.description = description;
		this.isPublic = isPublic;
		this.owner = owner;
	}

	public Owner getOwner()
	{
		return owner;
	}

	public String getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public boolean isPublic()
	{
		return isPublic;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(id)//
				.append(name)//
				.append(description)//
				.append(isPublic)//
				.append(owner)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final Playlist other = (Playlist) obj;
		return new EqualsBuilder()//
				.append(id, other.id)//
				.append(name, other.name)//
				.append(description, other.description)//
				.append(isPublic, other.isPublic)//
				.append(owner, other.owner)//
				.build();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("id", id)//
				.append("name", name)//
				.append("description", description)//
				.append("public", isPublic)//
				.append("owner", owner)//
				.build();
	}

}
