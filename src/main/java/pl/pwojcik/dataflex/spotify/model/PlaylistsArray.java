package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing list of Spotify user's playlists.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaylistsArray
{
	private final List<Playlist> items;
	private final Integer limit;
	private final Integer offset;
	private final Integer total;


	@JsonCreator
	public PlaylistsArray(@JsonProperty("items") final List<Playlist> items,//
			@JsonProperty("limit") final Integer limit,//
			@JsonProperty("offset") final Integer offset,//
			@JsonProperty("total") final Integer total)
	{
		this.items = items;
		this.limit = limit;
		this.offset = offset;
		this.total = total;
	}

	public List<Playlist> getItems()
	{
		return items;
	}

	public Integer getLimit()
	{
		return limit;
	}

	public Integer getOffset()
	{
		return offset;
	}

	public Integer getTotal()
	{
		return total;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(limit)//
				.append(offset)//
				.append(total)//
				.append(items)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final PlaylistsArray other = (PlaylistsArray) obj;
		return new EqualsBuilder()//
				.append(limit, other.limit)//
				.append(offset, other.offset)//
				.append(total, other.total)//
				.append(items, other.items)//
				.build();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("limit", limit)//
				.append("offset", offset)//
				.append("total", total)//
				.append("items", items)//
				.build();
	}
}
