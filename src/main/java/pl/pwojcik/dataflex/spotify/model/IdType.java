package pl.pwojcik.dataflex.spotify.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum IdType
{
	SHAREABLE, DIRECT
}
