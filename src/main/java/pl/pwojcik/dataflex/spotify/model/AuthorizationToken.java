package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Representation of access token used to communicate with Spotify API adapted to be stored in Redis.
 * See also: {@linkplain SpotifyAuthorizationToken}
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizationToken
{
	private final String accessToken;
	private final String refreshToken;
	private final long expirationTimestamp;

	@JsonCreator
	public AuthorizationToken(//
			@JsonProperty("accessToken") final String accessToken,//
			@JsonProperty("refreshToken") final String refreshToken, //
			@JsonProperty("expirationTimestamp") final long expirationTimestamp)
	{
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.expirationTimestamp = expirationTimestamp;
	}

	public String getAccessToken()
	{
		return accessToken;
	}

	public String getRefreshToken()
	{
		return refreshToken;
	}

	public long getExpirationTimestamp()
	{
		return expirationTimestamp;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("accessToken", accessToken)//
				.append("refreshToken", refreshToken)//
				.append("expirationTimestamp", expirationTimestamp).build();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(accessToken)//
				.append(refreshToken)//
				.append(expirationTimestamp)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final AuthorizationToken other = (AuthorizationToken) obj;
		return new EqualsBuilder()//
				.append(accessToken, other.accessToken)//
				.append(refreshToken, other.refreshToken)//
				.append(expirationTimestamp, other.expirationTimestamp)//
				.build();
	}

	@JsonIgnore
	public static AuthorizationToken from(final SpotifyAuthorizationToken token)
	{
		return new AuthorizationToken(token.getAccessToken(), token.getRefreshToken(), token.getExpirationTimestamp());
	}
}
