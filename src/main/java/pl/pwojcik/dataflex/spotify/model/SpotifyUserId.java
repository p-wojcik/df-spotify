package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing ID of user in Spotify.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpotifyUserId
{
	public static final String ACCOUNT_TYPE_PREMIUM = "premium";

	private final String id;
	private final String accountType;

	public SpotifyUserId(final String id)
	{
		this.id = id;
		this.accountType = "free";
	}

	@JsonCreator
	public SpotifyUserId(//
			@JsonProperty("id") final String id,//
			@JsonProperty("product") final String accountType)
	{
		this.id = id;
		this.accountType = accountType;
	}

	public String getId()
	{
		return id;
	}

	public String getAccountType()
	{
		return accountType;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(id)//
				.append(accountType)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final SpotifyUserId other = (SpotifyUserId) obj;
		return new EqualsBuilder()//
				.append(id, other.id)//
				.append(accountType, other.accountType)//
				.build();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("id", id)//
				.append("accountType", accountType)//
				.build();
	}
}
