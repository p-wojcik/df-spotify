package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing device on which user can play Spotify content.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Device
{
	private final String id;
	private final String name;
	private final String type;
	private final boolean active;
	private final int volumePercent;

	@JsonCreator
	public Device(//
			@JsonProperty("id") final String id, //
			@JsonProperty("name") final String name,//
			@JsonProperty("type") final String type,//
			@JsonProperty("is_active") final boolean active,//
			@JsonProperty("volume_percent") final int volumePercent)
	{
		this.id = id;
		this.name = name;
		this.type = type;
		this.active = active;
		this.volumePercent = volumePercent;
	}

	public String getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public String getType()
	{
		return type;
	}

	public boolean isActive()
	{
		return active;
	}

	public int getVolumePercent()
	{
		return volumePercent;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(id)//
				.append(name)//
				.append(type)//
				.append(active)//
				.append(volumePercent)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final Device other = (Device) obj;
		return new EqualsBuilder()//
				.append(id, other.id)//
				.append(name, other.name)//
				.append(type, other.type)//
				.append(active, other.active)//
				.append(volumePercent, other.volumePercent)//
				.build();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("id", id)//
				.append("name", name)//
				.append("type", type)//
				.append("active", active)//
				.append("volumePercent", volumePercent)//
				.build();
	}
}
