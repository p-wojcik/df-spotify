package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing single track/context that can be played on Spotify-compliant device.
 * "Context" means other type of Spotify's entities, like album, playlist, etc..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaybackTarget
{
	private final String id;
	private final IdType type;
	private final Target target;

	@JsonCreator
	public PlaybackTarget(//
			@JsonProperty("id") final String id,//
			@JsonProperty("type") final IdType type,//
			@JsonProperty("target") final Target target
	)
	{
		this.id = id;
		this.type = type;
		this.target = target;
	}

	public String getId()
	{
		return id;
	}

	public IdType getType()
	{
		return type;
	}

	public Target getTarget()
	{
		return target;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(id)//
				.append(type)//
				.append(target)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final PlaybackTarget other = (PlaybackTarget) obj;
		return new EqualsBuilder()//
				.append(id, other.id)//
				.append(type, other.type)//
				.append(target, other.target)//
				.build();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("id", id)//
				.append("type", type)//
				.append("target", target)//
				.build();
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	public enum Target
	{
		TRACK, CONTEXT
	}
}
