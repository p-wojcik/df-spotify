package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Representation of access token used to communicate with Spotify API adapted to be deserialized from HTTP response
 * coming from Spotify API.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpotifyAuthorizationToken
{
	private final String accessToken;
	private final String tokenType;
	private final String refreshToken;
	private final List<String> scopes;
	private final long expirationTimestamp;

	@JsonCreator
	public SpotifyAuthorizationToken(//
			@JsonProperty("access_token") final String accessToken,//
			@JsonProperty("token_type") final String tokenType,//
			@JsonProperty("refresh_token") final String refreshToken,//
			@JsonProperty("scope") final String scope,//
			@JsonProperty("expires_in") final int expiresIn)
	{
		this.accessToken = accessToken;
		this.tokenType = tokenType;
		this.refreshToken = refreshToken;
		this.scopes = List.of(scope.split(" "));
		this.expirationTimestamp = Instant.now().plus(expiresIn, ChronoUnit.SECONDS).toEpochMilli();
	}

	public String getAccessToken()
	{
		return accessToken;
	}

	public String getTokenType()
	{
		return tokenType;
	}

	public String getRefreshToken()
	{
		return refreshToken;
	}

	public List<String> getScopes()
	{
		return scopes;
	}

	public long getExpirationTimestamp()
	{
		return expirationTimestamp;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("accessToken", accessToken)//
				.append("tokenType", tokenType)//
				.append("refreshToken", refreshToken)//
				.append("scopes", String.join(" ", scopes))//
				.append("expirationTimestamp", expirationTimestamp)//
				.build();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(accessToken)//
				.append(tokenType)//
				.append(refreshToken)//
				.append(scopes)//
				.append(expirationTimestamp)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final SpotifyAuthorizationToken other = (SpotifyAuthorizationToken) obj;
		return new EqualsBuilder()//
				.append(accessToken, other.accessToken)//
				.append(tokenType, other.tokenType)//
				.append(refreshToken, other.refreshToken)//
				.append(scopes, other.scopes)//
				.append(expirationTimestamp, other.expirationTimestamp)//
				.build();
	}
}
