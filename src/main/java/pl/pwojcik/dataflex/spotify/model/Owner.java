package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing owner of Spotify playlist.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Owner
{
	private final String id;
	private final String displayName;

	@JsonCreator
	public Owner(//
			@JsonProperty("id") final String id,//
			@JsonProperty("display_name") final String displayName)
	{
		this.id = id;
		this.displayName = displayName;
	}

	public String getId()
	{
		return id;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(id)//
				.append(displayName)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final Owner other = (Owner) obj;
		return new EqualsBuilder()//
				.append(id, other.id)//
				.append(displayName, other.displayName)//
				.build();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("id", id)//
				.append("displayName", displayName)//
				.build();
	}
}
