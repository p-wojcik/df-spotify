package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing request of creating user's playlist in Spotify.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreatePlaylistRequest
{
	private final String name;
	private final String description;
	private final boolean isPublic;

	@JsonCreator
	public CreatePlaylistRequest(//
			@JsonProperty("name") final String name,//
			@JsonProperty("description") final String description,//
			@JsonProperty("public") final boolean isPublic)
	{
		this.name = name;
		this.description = isEmpty(description) ? "-" : description;
		this.isPublic = isPublic;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public boolean isPublic()
	{
		return isPublic;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(name)//
				.append(description)//
				.append(isPublic)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final CreatePlaylistRequest other = (CreatePlaylistRequest) obj;
		return new EqualsBuilder()//
				.append(name, other.name)//
				.append(description, other.description)//
				.append(isPublic, other.isPublic)//
				.build();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("name", name)//
				.append("description", description)//
				.append("isPublic", isPublic)//
				.build();
	}
}
