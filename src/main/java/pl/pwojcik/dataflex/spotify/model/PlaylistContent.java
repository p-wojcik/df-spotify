package pl.pwojcik.dataflex.spotify.model;

import static java.util.Objects.isNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class representing single track added to Spotify playlist.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaylistContent
{
	private final String id;
	private final IdType type;

	@JsonCreator
	public PlaylistContent(//
			@JsonProperty("id") final String id,//
			@JsonProperty("type") final IdType type)
	{
		this.id = id;
		this.type = type;
	}

	public String getId()
	{
		return id;
	}

	public IdType getType()
	{
		return type;
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(id)//
				.append(type)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (isNull(obj) || getClass() != obj.getClass())
		{
			return false;
		}

		final PlaylistContent other = (PlaylistContent) obj;
		return new EqualsBuilder()//
				.append(id, other.id)//
				.append(type, other.type)//
				.build();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
				.append("id", id)//
				.append("type", type)//
				.build();
	}

}
