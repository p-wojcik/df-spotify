package pl.pwojcik.dataflex.spotify.validator;

import pl.pwojcik.dataflex.spotify.exception.ValidationException;
import pl.pwojcik.dataflex.spotify.model.CreatePlaylistRequest;
import pl.pwojcik.dataflex.spotify.model.Volume;

/**
 * Validator of business requirements for {@linkplain Volume} instances.
 */
public class VolumeValidator
{
	private static final int MIN_VOLUME = 0;
	private static final int MAX_VOLUME = 100;
	private static final String ERROR_MESSAGE = String.format("Device volume must a value of range [%d, %d].", //
			MIN_VOLUME, MAX_VOLUME);

	/**
	 * Validates instance of {@linkplain CreatePlaylistRequest} class.
	 *
	 * @param volume instance to be validated
	 * @throws ValidationException in case validation fails
	 */
	public static void validate(final Volume volume)
	{
		final int volumePercent = volume.getVolumePercent();
		if (volumePercent < MIN_VOLUME || volumePercent > MAX_VOLUME)
		{
			throw new ValidationException(ERROR_MESSAGE);
		}
	}
}
