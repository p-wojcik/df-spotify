package pl.pwojcik.dataflex.spotify.validator;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.pwojcik.dataflex.spotify.exception.ValidationException;
import pl.pwojcik.dataflex.spotify.model.IdType;
import pl.pwojcik.dataflex.spotify.model.PlaylistContent;

/**
 * Validator of business requirements for {@linkplain PlaylistContent} instances.
 */
public class PlaylistContentValidator
{
	private static final Pattern DIRECT_ID_PATTERN = Pattern.compile("\\w+");

	/**
	 * Validates instance of {@linkplain PlaylistContent} class.
	 *
	 * @param content instance to be validated
	 * @throws ValidationException in case validation fails
	 */
	public static void validate(final PlaylistContent content)
	{
		if (content.getType() == IdType.DIRECT)
		{
			verifyDirectId(content);
		}
		else
		{
			verifyShareableId(content);
		}
	}

	private static void verifyDirectId(final PlaylistContent content)
	{
		final Matcher matcher = DIRECT_ID_PATTERN.matcher(content.getId());
		if (!matcher.matches())
		{
			throw new ValidationException("DirectID representing playlist content has invalid format: " + content.getId());
		}
	}

	private static void verifyShareableId(final PlaylistContent content)
	{
		try
		{
			UUID.fromString(content.getId());
		}
		catch (final IllegalArgumentException e)
		{
			throw new ValidationException("ShareableID representing playlist content has invalid format: " + content.getId());
		}
	}
}