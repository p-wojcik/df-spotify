package pl.pwojcik.dataflex.spotify.validator;

import static org.apache.commons.lang3.StringUtils.isBlank;

import pl.pwojcik.dataflex.spotify.exception.ValidationException;
import pl.pwojcik.dataflex.spotify.model.CreatePlaylistRequest;

/**
 * Validator of business requirements for {@linkplain CreatePlaylistRequest} instances.
 */
public class CreatePlaylistRequestValidator
{
	/**
	 * Validates instance of {@linkplain CreatePlaylistRequest} class.
	 * @param request instance to be validated
	 * @throws ValidationException in case validation fails
	 */
	public static void validate(final CreatePlaylistRequest request)
	{
		if (isBlank(request.getName()))
		{
			throw new ValidationException("Playlist name cannot be empty or null.");
		}
	}
}
