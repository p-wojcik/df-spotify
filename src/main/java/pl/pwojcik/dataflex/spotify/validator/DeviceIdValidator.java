package pl.pwojcik.dataflex.spotify.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * Validator of DeviceID.
 */
public class DeviceIdValidator
{
	private static final Pattern DEVICE_ID_PATTERN = Pattern.compile("\\w+");

	/**
	 * Validates deviceIDs.
	 *
	 * @param deviceId ID to be validated
	 * @return true if ID is valid
	 */
	public static boolean isValid(final String deviceId)
	{
		if (StringUtils.isBlank(deviceId))
		{
			return true;
		}
		final Matcher matcher = DEVICE_ID_PATTERN.matcher(deviceId);
		return matcher.matches();
	}
}
