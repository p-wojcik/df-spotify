package pl.pwojcik.dataflex.spotify.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.handler.codec.http.HttpStatusClass;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.model.SpotifyAuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.SpotifyUserId;

/**
 * Service responsible for fetching from Spotify user ID along with account type.
 */
public class UserIdFetchingService
{
	private static final Logger LOG = LogManager.getLogger(UserIdFetchingService.class);
	private static final String CONFIG_PROPERTY_API_ROOT = "spotify.api.url";
	private static final String SPOTIFY_USER_DATA_URL = "/me";

	private final JsonObject config;
	private final WebClient webClient;

	public UserIdFetchingService(final WebClient webClient, final JsonObject config)
	{
		this.webClient = webClient;
		this.config = config;
	}

	/**
	 * Calls Spotify with provided access token to obtain information about userID and accountType.
	 *
	 * @param spotifyAuthorizationToken token
	 * @return userId + accountType
	 */
	public Promise<SpotifyUserId> getSpotifyUserId(final SpotifyAuthorizationToken spotifyAuthorizationToken)
	{
		final Promise<SpotifyUserId> userIdPromise = Promise.promise();
		final String uri = config.getString(CONFIG_PROPERTY_API_ROOT) + SPOTIFY_USER_DATA_URL;

		webClient.getAbs(uri)//
				.bearerTokenAuthentication(spotifyAuthorizationToken.getAccessToken())//
				.send(ar -> {
					if (ar.succeeded())
					{
						final HttpResponse<Buffer> idResponse = ar.result();
						final HttpStatusClass httpStatusClass = HttpStatusClass.valueOf(idResponse.statusCode());
						if (httpStatusClass == HttpStatusClass.SUCCESS)
						{
							final SpotifyUserId spotifyUserId = idResponse.bodyAsJson(SpotifyUserId.class);
							userIdPromise.complete(spotifyUserId);
						}
						else if (httpStatusClass == HttpStatusClass.CLIENT_ERROR)
						{
							LOG.error("Client error with code {} received while getting user ID from Spotify. " +//
									"Details: {}", idResponse.statusCode(), idResponse.bodyAsString());
							userIdPromise.fail("Client error while getting user details.");
						}
						else
						{
							LOG.error("Server error with code {} received while getting user ID from Spotify. " +//
									"Details: {}", idResponse.statusCode(), idResponse.bodyAsString());
							userIdPromise.fail("Internal server error while getting user details.");
						}
					}
					else
					{
						LOG.error("Connection error on getting user details: {}", ar.cause());
						userIdPromise.fail("Connection error on getting user details.");
					}
				});
		return userIdPromise;
	}
}
