package pl.pwojcik.dataflex.spotify.service;

import pl.pwojcik.dataflex.spotify.model.PlaybackTarget;
import pl.pwojcik.dataflex.spotify.model.PlaylistContent;

public class ShareableIdTranslator
{
	public String translateToDirectID(final PlaylistContent content)
	{
		// TODO translate[UNSUPPORTED] try to GET track from our local collection "spotify" using shareable ID
		throw new UnsupportedOperationException("ShareableID is currently not supported.");
	}

	public String translateToDirectID(final PlaybackTarget target)
	{
		// TODO translate[UNSUPPORTED] try to GET track from our local collection "spotify" using shareable ID
		throw new UnsupportedOperationException("ShareableID is currently not supported.");
	}
}
