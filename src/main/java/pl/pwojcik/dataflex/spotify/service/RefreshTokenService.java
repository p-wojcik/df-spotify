package pl.pwojcik.dataflex.spotify.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.handler.codec.http.HttpStatusClass;
import io.vertx.core.MultiMap;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.SpotifyAuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.SpotifyUserId;
import pl.pwojcik.dataflex.spotify.repository.SpotifyTokenRepository;

/**
 * Service responsible for performing Refresh Token flow for Spotify access token.
 */
public class RefreshTokenService
{
	private static final Logger LOG = LogManager.getLogger(RefreshTokenService.class);

	private static final String CONFIG_PROPERTY_TOKEN_URI = "spotify.auth.token.uri";
	private static final String CONFIG_PROPERTY_CLIENT_ID = "spotify.auth.basic.id";
	private static final String CONFIG_PROPERTY_CLIENT_SECRET = "spotify.auth.basic.secret";
	private static final String FORM_ATTR_GRANT_TYPE = "grant_type";
	private static final String FORM_ATTR_REFRESH_TOKEN = "refresh_token";
	private static final String FORM_ATTR_VALUE_REFRESH_TOKEN = "refresh_token";
	private final JsonObject config;
	private final WebClient webClient;
	private final SpotifyTokenRepository repository;
	private final UserIdFetchingService userIdFetchingService;

	public RefreshTokenService(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
		this.userIdFetchingService = toolbox.getUserIdFetchingService();
		this.repository = toolbox.getSpotifyTokenRepository();
	}

	/**
	 * Performs refresh token flow against Spotify auth API for user
	 *
	 * @param spotifyUserId spotify ID of user
	 * @param refreshToken refresh token of user
	 * @return true if refresh succeeded
	 */
	public Promise<Boolean> refreshToken(final SpotifyUserId spotifyUserId, final String refreshToken)
	{
		final Promise<Boolean> response = Promise.promise();
		final MultiMap form = prepareForm(refreshToken);
		final String clientId = config.getString(CONFIG_PROPERTY_CLIENT_ID);
		final String clientSecret = config.getString(CONFIG_PROPERTY_CLIENT_SECRET);
		webClient.postAbs(config.getString(CONFIG_PROPERTY_TOKEN_URI))//
				.basicAuthentication(clientId, clientSecret)//
				.sendForm(form, ar -> {
					if (ar.succeeded())
					{
						final HttpResponse<Buffer> tokenResponse = ar.result();
						final HttpStatusClass httpStatusClass = HttpStatusClass.valueOf(tokenResponse.statusCode());
						if (httpStatusClass == HttpStatusClass.SUCCESS)
						{
							// Here we have valid refreshed Authorization Bearer Token.
							final SpotifyAuthorizationToken spotifyAuthorizationToken = //
									tokenResponse.bodyAsJson(SpotifyAuthorizationToken.class);

							userIdFetchingService.getSpotifyUserId(spotifyAuthorizationToken).future()//
									.compose(id -> storeAuthorizationToken(spotifyAuthorizationToken, id).future())//
									.setHandler(arr -> response.complete(arr.succeeded()));
						}
						else
						{
							LOG.error("HTTP status code {} received while refreshing access token for user {}. Details: {}",
									tokenResponse.statusCode(), spotifyUserId, tokenResponse.bodyAsString());
							repository.deleteTokenForUser(spotifyUserId).future()//
									.setHandler(arr -> response.complete(arr.succeeded()));
						}
					}
					else
					{
						LOG.error("Connection error on refreshing access token for user {}: {}", spotifyUserId, ar.cause());
						response.fail(ar.cause());
					}
				});
		return response;
	}

	private MultiMap prepareForm(final String refreshToken)
	{
		final MultiMap form = MultiMap.caseInsensitiveMultiMap();
		form.add(FORM_ATTR_GRANT_TYPE, FORM_ATTR_VALUE_REFRESH_TOKEN);
		form.add(FORM_ATTR_REFRESH_TOKEN, refreshToken);
		return form;
	}

	private Promise<Boolean> storeAuthorizationToken(final SpotifyAuthorizationToken spotifyAuthorizationToken,
			final SpotifyUserId userId)
	{
		final Promise<Boolean> promise = Promise.promise();
		final AuthorizationToken localAuthToken = AuthorizationToken.from(spotifyAuthorizationToken);
		repository.storeTokenForUser(userId, localAuthToken).future()//
				.setHandler(arr -> {
					if (arr.succeeded())
					{
						promise.complete(true);
					}
					else
					{
						promise.fail(arr.cause());
					}
				});
		return promise;
	}
}
