package pl.pwojcik.dataflex.spotify.web.authorization;

import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithFound;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithNoContent;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithUnauthorized;
import static pl.pwojcik.dataflex.spotify.web.authorization.AuthorizationRequestForwarder.SESSION_PARAM_STATE;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.handler.codec.http.HttpStatusClass;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.SpotifyAuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.SpotifyUserId;
import pl.pwojcik.dataflex.spotify.repository.SpotifyTokenRepository;
import pl.pwojcik.dataflex.spotify.service.UserIdFetchingService;

/**
 * Handler responsible for:
 * - fetching authorization code from query parameters,
 * - exchanging authorization code for Authorization token,
 * - retrieving user's Spotify ID,
 * - caching user ID and token in Redis.
 */
public class AuthorizationCodeHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(AuthorizationCodeHandler.class);

	private static final String CONFIG_PROPERTY_REDIRECT_URI = "spotify.auth.redirect.uri";
	private static final String CONFIG_PROPERTY_TOKEN_URI = "spotify.auth.token.uri";
	private static final String CONFIG_PROPERTY_CLIENT_ID = "spotify.auth.basic.id";
	private static final String CONFIG_PROPERTY_CLIENT_SECRET = "spotify.auth.basic.secret";
	private static final String FORM_ATTR_GRANT_TYPE = "grant_type";
	private static final String FORM_ATTR_CODE = "code";
	private static final String FORM_ATTR_REDIRECT_URI = "redirect_uri";
	private static final String FORM_ATTR_VALUE_AUTHORIZATION_CODE = "authorization_code";
	private static final String QUERY_PARAM_CODE = "code";
	private static final String QUERY_PARAM_STATE = "state";

	private static final int FIRST_ELEMENT_INDEX = 0;

	private final JsonObject config;
	private final WebClient webClient;
	private final UserIdFetchingService userIdFetchingService;
	private final SpotifyTokenRepository repository;

	public AuthorizationCodeHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
		this.userIdFetchingService = toolbox.getUserIdFetchingService();
		this.repository = toolbox.getSpotifyTokenRepository();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerResponse response = event.response();
		final Session session = event.session();
		final String sessionStateParam = session.get(SESSION_PARAM_STATE);
		final String stateQP = event.queryParam(QUERY_PARAM_STATE).stream()//
				.findFirst()//
				.orElse("");

		if (StringUtils.isNotBlank(sessionStateParam) && sessionStateParam.equals(stateQP))
		{
			final List<String> codeQueryParamList = event.queryParam(QUERY_PARAM_CODE);
			if (!codeQueryParamList.isEmpty())
			{
				final String clientId = config.getString(CONFIG_PROPERTY_CLIENT_ID);
				final String clientSecret = config.getString(CONFIG_PROPERTY_CLIENT_SECRET);
				final MultiMap form = prepareForm(event);

				webClient.postAbs(config.getString(CONFIG_PROPERTY_TOKEN_URI))//
						.basicAuthentication(clientId, clientSecret)//
						.sendForm(form, ar -> {
							if (ar.succeeded())
							{
								final HttpResponse<Buffer> tokenResponse = ar.result();
								final HttpStatusClass httpStatusClass = HttpStatusClass.valueOf(tokenResponse.statusCode());
								if (httpStatusClass == HttpStatusClass.SUCCESS)
								{
									// Here we have valid Authorization Bearer Token.
									final SpotifyAuthorizationToken spotifyAuthorizationToken = //
											tokenResponse.bodyAsJson(SpotifyAuthorizationToken.class);
									LOG.debug("Successfully retrieved access token from Spotify.");
									userIdFetchingService.getSpotifyUserId(spotifyAuthorizationToken).future()//
											.setHandler(userIdResult -> {
												if (userIdResult.succeeded())
												{
													// Here we have valid Spotify user ID
													final SpotifyUserId userId = userIdResult.result();
													LOG.debug("Successfully retrieved user ID from Spotify: {}", userId);
													storeAuthorizationToken(response, spotifyAuthorizationToken, userId);
												}
												else
												{
													LOG.error("User ID could not be fetched: {}", userIdResult.cause());
													respondWithInternalError(response);
												}
											});
								}
								else if (httpStatusClass == HttpStatusClass.CLIENT_ERROR)
								{
									LOG.error("HTTP status code {} received while getting access token. Details: {}",
											tokenResponse.statusCode(), tokenResponse.bodyAsString());
									respondWithUnauthorized(response);
								}
								else
								{
									LOG.error("HTTP status code {} received while getting access token. Details: {}",
											tokenResponse.statusCode(), tokenResponse.bodyAsString());
									respondWithInternalError(response);
								}
							}
							else
							{
								LOG.error("Connection error on getting access token: {}", ar.cause());
								respondWithInternalError(response);
							}
						});
			}
			else
			{
				respondWithNoContent(response);
			}
		}
		else
		{
			LOG.error("Authentication failed. Invalid state parameter.");
			respondWithUnauthorized(response);
		}
	}

	private MultiMap prepareForm(final RoutingContext event)
	{
		final HttpServerRequest request = event.request();
		final List<String> codeQueryParamList = event.queryParam(QUERY_PARAM_CODE);
		final String authorizationCode = codeQueryParamList.get(FIRST_ELEMENT_INDEX);
		final String redirectUri = request.scheme() + "://" + request.host() + request.path();
		final MultiMap form = MultiMap.caseInsensitiveMultiMap();
		form.add(FORM_ATTR_GRANT_TYPE, FORM_ATTR_VALUE_AUTHORIZATION_CODE);
		form.add(FORM_ATTR_CODE, authorizationCode);
		form.add(FORM_ATTR_REDIRECT_URI, redirectUri);
		return form;
	}

	private void storeAuthorizationToken(final HttpServerResponse response,
			final SpotifyAuthorizationToken spotifyAuthorizationToken, final SpotifyUserId userId)
	{
		final AuthorizationToken localAuthToken = AuthorizationToken.from(spotifyAuthorizationToken);
		repository.storeTokenForUser(userId, localAuthToken).future()//
				.setHandler(arr -> {
					if (arr.succeeded())
					{
						final String location = config.getString(CONFIG_PROPERTY_REDIRECT_URI);
						respondWithFound(response, location);
					}
					else
					{
						respondWithInternalError(response);
					}
				});
	}
}
