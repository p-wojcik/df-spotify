package pl.pwojcik.dataflex.spotify.web.devices;

import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithBadRequest;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithNoContent;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.DEVICE_ID_PARAMETER;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.Volume;
import pl.pwojcik.dataflex.spotify.validator.VolumeValidator;

/**
 * Class responsible for setting volume on target device.
 */
public class VolumeHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(VolumeHandler.class);
	private static final String CONFIG_PROPERTY_SPOTIFY_API_ROOT = "spotify.api.url";
	private static final String SPOTIFY_VOLUME_URI = "me/player/volume";
	private static final String QUERY_PARAM_VOLUME_PERCENT = "volume_percent";
	private static final String QUERY_PARAM_DEVICE_ID = "device_id";

	private final WebClient webClient;
	private final JsonObject config;

	public VolumeHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerRequest request = event.request();
		final HttpServerResponse response = event.response();
		final String userId = request.getParam(USER_ID_PARAMETER);
		final String deviceId = event.pathParam(DEVICE_ID_PARAMETER);
		final String apiRoot = config.getString(CONFIG_PROPERTY_SPOTIFY_API_ROOT);
		final String uri = String.join("/", apiRoot, SPOTIFY_VOLUME_URI);
		final AuthorizationToken token = event.get(userId);

		try
		{
			final Volume volume = Json.decodeValue(event.getBody(), Volume.class);
			VolumeValidator.validate(volume);

			LOG.debug("Setting volume on device for user {}: {}", userId, volume.toString());
			webClient.putAbs(uri)//
					.putHeader(HttpHeaderNames.CONTENT_LENGTH.toString(), "0")//
					.bearerTokenAuthentication(token.getAccessToken())//
					.addQueryParam(QUERY_PARAM_VOLUME_PERCENT, String.valueOf(volume.getVolumePercent()))//
					.addQueryParam(QUERY_PARAM_DEVICE_ID, deviceId)//
					.send(ar -> {
						if (ar.succeeded())
						{
							final HttpResponse<Buffer> apiResponse = ar.result();
							if (apiResponse.statusCode() == HttpResponseStatus.NO_CONTENT.code())
							{
								respondWithNoContent(response);
							}
							else
							{
								LOG.error("Error while setting volume {} on device for user {}. HTTP code: {}. Details: {}", //
										volume, userId, apiResponse.statusCode(), apiResponse.bodyAsString());
								respondWithInternalError(response);
							}
						}
						else
						{
							LOG.error("Connection error while setting volume {} on device for user {}. Details: {}", //
									volume, userId, ar.cause());
							respondWithInternalError(response);
						}
					});
		}
		catch (final DecodeException e)
		{
			LOG.error("Error while decoding volume settings JSON for user {}: {}. Cause: {}", userId, event.getBodyAsString(), e);
			respondWithBadRequest(response, "Given JSON is not valid playlist representation.");
		}
	}
}
