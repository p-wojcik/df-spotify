package pl.pwojcik.dataflex.spotify.web;

import static io.netty.handler.codec.http.HttpResponseStatus.INTERNAL_SERVER_ERROR;
import static io.vertx.core.http.HttpMethod.DELETE;
import static io.vertx.core.http.HttpMethod.GET;
import static io.vertx.core.http.HttpMethod.HEAD;
import static io.vertx.core.http.HttpMethod.OPTIONS;
import static io.vertx.core.http.HttpMethod.POST;
import static io.vertx.core.http.HttpMethod.PUT;

import com.google.common.collect.ImmutableSet;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.web.authorization.AuthorizationCodeHandler;
import pl.pwojcik.dataflex.spotify.web.authorization.AuthorizationInterceptingHandler;
import pl.pwojcik.dataflex.spotify.web.authorization.AuthorizationRequestForwarder;
import pl.pwojcik.dataflex.spotify.web.authorization.AuthorizationSink;
import pl.pwojcik.dataflex.spotify.web.devices.GetDevicesHandler;
import pl.pwojcik.dataflex.spotify.web.devices.VolumeHandler;
import pl.pwojcik.dataflex.spotify.web.playback.PauseHandler;
import pl.pwojcik.dataflex.spotify.web.playback.PlaybackHandler;
import pl.pwojcik.dataflex.spotify.web.playback.TransferPlaybackHandler;
import pl.pwojcik.dataflex.spotify.web.playlist.CreatePlaylistHandler;
import pl.pwojcik.dataflex.spotify.web.playlist.FollowPlaylistHandler;
import pl.pwojcik.dataflex.spotify.web.playlist.GetPlaylistsHandler;
import pl.pwojcik.dataflex.spotify.web.playlist.PublishPlaylistHandler;

/**
 * Class responsible for binding HTTP handlers to routes.
 */
public class RoutesProvider
{
	public static final String USER_ID_PARAMETER = "userId";
	public static final String DEVICE_ID_PARAMETER = "deviceId";
	public static final String PLAYLIST_ID_PARAMETER = "playlistId";

	private static final String UI_URL_PROPERTY_NAME = "ui.url";

	private static final String PLAYLISTS_PATH = "/users/:userId/playlists";
	private static final String PLAYLIST_BY_ID_PATH = "/users/:userId/playlists/:playlistId";
	private static final String FOLLOW_PLAYLIST_BY_ID_PATH = "/users/:userId/playlists/:playlistId/follow";
	private static final String PUBLISH_PLAYLIST_BY_ID_PATH = "/users/:userId/playlists/:playlistId/publish";

	private static final String DEVICES_PATH = "/users/:userId/devices";
	private static final String DEVICE_VOLUME_PATH = "/users/:userId/devices/:deviceId";

	private static final String PLAYBACK_PATH = "/users/:userId/playback";
	private static final String PAUSE_PLAYBACK_PATH = "/users/:userId/playback/pause";
	private static final String TRANSFER_PLAYBACK_PATH = "/users/:userId/playback/transfer";

	private static final String AUTHORIZATION_CODE_FLOW_REDIRECT_PATH = "/redirect";
	private static final String AUTHORIZATION_CODE_FLOW_AUTH_PATH = "/authorize";
	private static final String INTRODUCE_PATH = "/users/:userId/introduce";

	public static void bindRoutes(final Router router, final Toolbox toolbox, final Vertx vertx)
	{
		final AuthorizationInterceptingHandler authInterceptor = new AuthorizationInterceptingHandler(toolbox);

		bindAuthorizationRoutes(router, toolbox, vertx, authInterceptor);
		bindPlaylistRoutes(router, toolbox, authInterceptor);
		bindDevicesRoutes(router, toolbox, authInterceptor);
		bindPlaybackRoutes(router, toolbox, authInterceptor);
		bindCommonRoutes(router, toolbox);
	}

	private static void bindAuthorizationRoutes(final Router router, final Toolbox toolbox, final Vertx vertx,//
			final AuthorizationInterceptingHandler authInterceptor)
	{
		final SessionHandler sessionHandler = SessionHandler.create(LocalSessionStore.create(vertx));

		// UI-related endpoint for validating if user is already logged in
		router.head(INTRODUCE_PATH)//
			.handler(authInterceptor)//
			.handler(new AuthorizationSink());

		// Authorization forwarding - add 'state' query parameter
		router.get(AUTHORIZATION_CODE_FLOW_AUTH_PATH)//
				.handler(sessionHandler)//
				.handler(new AuthorizationRequestForwarder(toolbox));

		// Redirect handler for Auth flow
		router.get(AUTHORIZATION_CODE_FLOW_REDIRECT_PATH)//
				.handler(sessionHandler)//
				.handler(new AuthorizationCodeHandler(toolbox));
	}

	private static void bindPlaylistRoutes(final Router router, final Toolbox toolbox,
			final AuthorizationInterceptingHandler authInterceptor)
	{
		// Get all user playlists
		router.get(PLAYLISTS_PATH)//
				.handler(authInterceptor)//
				.handler(new GetPlaylistsHandler(toolbox));

		// Create playlist for user
		router.post(PLAYLISTS_PATH)//
				.handler(BodyHandler.create())//
				.handler(authInterceptor)//
				.handler(new CreatePlaylistHandler(toolbox));

		final FollowPlaylistHandler followPlaylistHandler = new FollowPlaylistHandler(toolbox);

		// Delete user's playlist
		router.delete(PLAYLIST_BY_ID_PATH)//
				.handler(authInterceptor)//
				.handler(followPlaylistHandler);

		// Follow playlist
		router.put(FOLLOW_PLAYLIST_BY_ID_PATH)//
				.handler(authInterceptor)//
				.handler(followPlaylistHandler);

		// Unfollow playlist
		router.delete(FOLLOW_PLAYLIST_BY_ID_PATH)//
				.handler(authInterceptor)//
				.handler(followPlaylistHandler);

		// Update user's playlist to public
		final PublishPlaylistHandler publishPlaylistHandler = new PublishPlaylistHandler(toolbox);
		router.put(PUBLISH_PLAYLIST_BY_ID_PATH)//
				.handler(authInterceptor)//
				.handler(publishPlaylistHandler);

		// Update user's playlist to private
		router.delete(PUBLISH_PLAYLIST_BY_ID_PATH)//
				.handler(authInterceptor)//
				.handler(publishPlaylistHandler);
	}

	private static void bindDevicesRoutes(final Router router, final Toolbox toolbox,
			final AuthorizationInterceptingHandler authInterceptor)
	{
		// Get all user devices
		router.get(DEVICES_PATH)//
				.handler(authInterceptor)//
				.handler(new GetDevicesHandler(toolbox));

		// Set volume on device
		router.put(DEVICE_VOLUME_PATH)//
				.handler(BodyHandler.create())//
				.handler(authInterceptor)//
				.handler(new VolumeHandler(toolbox));
	}

	private static void bindPlaybackRoutes(final Router router, final Toolbox toolbox,
			final AuthorizationInterceptingHandler authInterceptor)
	{
		// Transfer playback to another device
		router.put(TRANSFER_PLAYBACK_PATH)//
				.handler(authInterceptor)//
				.handler(new TransferPlaybackHandler(toolbox));

		// Start/resume playback
		router.put(PLAYBACK_PATH)//
				.handler(BodyHandler.create())//
				.handler(authInterceptor)//
				.handler(new PlaybackHandler(toolbox));

		// Pause playback
		router.put(PAUSE_PLAYBACK_PATH)//
				.handler(authInterceptor)//
				.handler(new PauseHandler(toolbox));
	}

	private static void bindCommonRoutes(final Router router, final Toolbox toolbox)
	{
		// Error handling
		router.errorHandler(INTERNAL_SERVER_ERROR.code(), new ExceptionHandler());

		// CORS handling
		router.route()//
				.handler(//
						CorsHandler.create(toolbox.getConfig().getString(UI_URL_PROPERTY_NAME))//
								.allowedMethods(ImmutableSet.of(GET, POST, HEAD, PUT, DELETE, OPTIONS))//
								.allowCredentials(true)//
								.allowedHeader(HttpHeaderNames.ACCESS_CONTROL_ALLOW_METHODS.toString())//
								.allowedHeader(HttpHeaderNames.ACCESS_CONTROL_ALLOW_ORIGIN.toString())//
								.allowedHeader(HttpHeaderNames.ACCESS_CONTROL_ALLOW_CREDENTIALS.toString())//
								.allowedHeader(HttpHeaderNames.CONTENT_TYPE.toString())//
				);
	}
}
