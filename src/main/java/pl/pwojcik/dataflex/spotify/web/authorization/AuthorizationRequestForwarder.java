package pl.pwojcik.dataflex.spotify.web.authorization;

import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithFound;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.UUID;

import com.google.common.collect.ImmutableMap;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import pl.pwojcik.dataflex.spotify.Toolbox;

/**
 * Handler responsible for making authorization request for OAuth2 Authorization Code flow.
 */
public class AuthorizationRequestForwarder implements Handler<RoutingContext>
{
	static final String SESSION_PARAM_STATE = "authCodeState";

	private static final String QUERY_PARAM_RESPONSE_TYPE = "response_type";
	private static final String QUERY_PARAM_CLIENT_ID = "client_id";
	private static final String QUERY_PARAM_SCOPE = "scope";
	private static final String QUERY_PARAM_REDIRECT_URI = "redirect_uri";
	private static final String QUERY_PARAM_SHOW_DIALOG = "show_dialog";
	private static final String QUERY_PARAM_STATE = "state";

	private static final String RESPONSE_TYPE_CODE = "code";
	private static final String SHOW_DIALOG_PARAM_VALUE = "true";

	private static final String CONFIG_PROPERTY_AUTHORIZATION_URI = "spotify.auth.authorization.uri";
	private static final String CONFIG_PROPERTY_REDIRECT_PATH = "spotify.auth.codeRedirect.path";
	private static final String CONFIG_PROPERTY_CLIENT_ID = "spotify.auth.basic.id";
	private static final String CONFIG_PROPERTY_SCOPE = "spotify.auth.scopelist";

	private final JsonObject config;

	public AuthorizationRequestForwarder(final Toolbox toolbox)
	{
		this.config = toolbox.getConfig();
	}

	@Override
	public void handle(final RoutingContext routingContext)
	{
		final Session session = routingContext.session();
		final String stateParam = UUID.randomUUID().toString();
		session.put(SESSION_PARAM_STATE, stateParam);

		final String locationWithoutQP = config.getString(CONFIG_PROPERTY_AUTHORIZATION_URI);
		final HttpServerRequest request = routingContext.request();
		final Map<String, String> queryParameters = prepareQueryParametersMap(request, stateParam);
		final String queryString = translateToQueryString(queryParameters);
		final String fullLocation = locationWithoutQP + "?" + queryString;
		respondWithFound(routingContext.response(), fullLocation);
	}

	private Map<String, String> prepareQueryParametersMap(final HttpServerRequest request, final String stateParam)
	{
		final String redirectUri = request.scheme() + "://" + request.host() + config.getString(CONFIG_PROPERTY_REDIRECT_PATH);

		return ImmutableMap.<String, String>builder()//
				.put(QUERY_PARAM_RESPONSE_TYPE, RESPONSE_TYPE_CODE)//
				.put(QUERY_PARAM_CLIENT_ID, config.getString(CONFIG_PROPERTY_CLIENT_ID))//
				.put(QUERY_PARAM_SCOPE, config.getString(CONFIG_PROPERTY_SCOPE))//
				.put(QUERY_PARAM_REDIRECT_URI, redirectUri)//
				.put(QUERY_PARAM_SHOW_DIALOG, SHOW_DIALOG_PARAM_VALUE)//
				.put(QUERY_PARAM_STATE, stateParam)//
				.build();
	}

	private String translateToQueryString(final Map<String, String> queryParams)
	{
		final StringBuilder builder = new StringBuilder();
		for (final Map.Entry<String, String> e : queryParams.entrySet())
		{
			if (builder.length() > 0)
			{
				builder.append('&');
			}
			builder.append(URLEncoder.encode(e.getKey(), StandardCharsets.UTF_8))//
					.append('=')//
					.append(URLEncoder.encode(e.getValue(), StandardCharsets.UTF_8));//
		}
		return builder.toString();
	}
}
