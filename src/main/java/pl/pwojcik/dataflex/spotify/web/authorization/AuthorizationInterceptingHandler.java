package pl.pwojcik.dataflex.spotify.web.authorization;

import static pl.pwojcik.dataflex.spotify.model.SpotifyUserId.ACCOUNT_TYPE_PREMIUM;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithUnauthorized;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import java.time.Instant;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.tuple.Pair;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.exception.NoAccessTokenCached;
import pl.pwojcik.dataflex.spotify.exception.ValidationException;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.SpotifyUserId;
import pl.pwojcik.dataflex.spotify.repository.SpotifyTokenRepository;
import pl.pwojcik.dataflex.spotify.service.RefreshTokenService;

/**
 * Handler responsible for checking if there's access token cached for user with given ID.
 * This handler should be used before handlers responsible for executing business logic.
 */
public class AuthorizationInterceptingHandler implements Handler<RoutingContext>
{
	private static final Pattern USER_ID_PATTERN = Pattern.compile("\\w+");

	private final SpotifyTokenRepository repository;
	private final RefreshTokenService refreshService;

	public AuthorizationInterceptingHandler(final Toolbox toolbox)
	{
		this.repository = toolbox.getSpotifyTokenRepository();
		this.refreshService = new RefreshTokenService(toolbox);
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final String userId = event.pathParam(USER_ID_PARAMETER);
		verifyUserId(userId);
		final SpotifyUserId spotifyUserId = new SpotifyUserId(userId);
		repository.getTokenForUser(spotifyUserId).future()//
				.setHandler(ar -> {
					if (ar.succeeded())
					{
						final Pair<String, AuthorizationToken> result = ar.result();
						final String accountType = result.getLeft();
						if (accountType.equals(ACCOUNT_TYPE_PREMIUM))
						{
							final AuthorizationToken token = result.getRight();
							final Instant expirationDate = Instant.ofEpochMilli(token.getExpirationTimestamp());
							if (expirationDate.isBefore(Instant.now()))
							{
								// Token expired, Refresh it
								refreshService.refreshToken(spotifyUserId, token.getRefreshToken()).future()//
										.setHandler(arr -> {
											if (arr.succeeded())
											{
												event.put(userId, token);
												event.next();
											}
											else
											{
												respondWithUnauthorized(event.response());
											}
										});
							}
							else
							{
								event.put(userId, token);
								event.next();
							}
						}
						else
						{
							respondWithUnauthorized(event.response());
						}
					}
					else
					{
						// No data in Redis for user or Redis connection error
						if (ar.cause() instanceof NoAccessTokenCached)
						{
							respondWithUnauthorized(event.response());
						}
						else
						{
							respondWithInternalError(event.response());
						}
					}
				});
	}

	private void verifyUserId(final String userId)
	{
		final Matcher matcher = USER_ID_PATTERN.matcher(userId);
		if (!matcher.matches())
		{
			throw new ValidationException("Given UserID has invalid format: " + userId);
		}
	}
}
