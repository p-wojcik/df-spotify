package pl.pwojcik.dataflex.spotify.web.playlist;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithNoContent;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.PLAYLIST_ID_PARAMETER;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;

/**
 * Handler for deleting / following / un-following playlists in Spotify.
 * In Spotify plylist delete & un-follow is the same operation!
 */
public class FollowPlaylistHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(FollowPlaylistHandler.class);
	private static final String CONFIG_PROPERTY_SPOTIFY_API_ROOT = "spotify.api.url";
	private static final String URI_PATH_PLAYLISTS = "playlists";
	private static final String URI_PATH_FOLLOWERS = "followers";

	private final WebClient webClient;
	private final JsonObject config;

	public FollowPlaylistHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerRequest request = event.request();
		final HttpServerResponse response = event.response();
		final String playlistId = request.getParam(PLAYLIST_ID_PARAMETER);
		final String apiRoot = config.getString(CONFIG_PROPERTY_SPOTIFY_API_ROOT);
		final String uri = String.join("/", apiRoot, URI_PATH_PLAYLISTS, playlistId, URI_PATH_FOLLOWERS);
		final String userId = event.pathParam(USER_ID_PARAMETER);
		final AuthorizationToken token = event.get(userId);
		final HttpMethod method = request.method();

		final String logMessageAction = getLogMessageAction(method);
		supplyRestCall(method, uri)//
				.bearerTokenAuthentication(token.getAccessToken())//
				.send(ar -> {
					if (ar.succeeded())
					{
						final HttpResponse<Buffer> apiResponse = ar.result();
						if (apiResponse.statusCode() == OK.code())
						{
							respondWithNoContent(response);
						}
						else
						{
							LOG.error("Error while {} playlist with ID {} for user {}. HTTP code {}. Details: {}", //
									logMessageAction, playlistId, userId, apiResponse.statusCode(), apiResponse.bodyAsString());
							respondWithInternalError(response);
						}
					}
					else
					{
						LOG.error("Connection error while {} playlist with ID {} for user {}. Cause: {}", //
								logMessageAction, playlistId, userId, ar.cause());
						respondWithInternalError(response);
					}
				});
	}

	private HttpRequest<Buffer> supplyRestCall(final HttpMethod method, final String uri)
	{
		if (HttpMethod.PUT == method)
		{
			return webClient.putAbs(uri).putHeader(HttpHeaderNames.CONTENT_LENGTH.toString(), "0");
		}
		return webClient.deleteAbs(uri);
	}

	private String getLogMessageAction(final HttpMethod method)
	{
		if (HttpMethod.PUT == method)
		{
			return "following";
		}
		return "deleting/un-following";
	}
}
