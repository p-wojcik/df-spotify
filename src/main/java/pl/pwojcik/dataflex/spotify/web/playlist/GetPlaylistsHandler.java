package pl.pwojcik.dataflex.spotify.web.playlist;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithOk;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.PlaylistsArray;

/**
 * Handler responsible for getting users playlists from Spotify.
 */
public class GetPlaylistsHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(GetPlaylistsHandler.class);
	private static final String CONFIG_PROPERTY_SPOTIFY_API_ROOT = "spotify.api.url";
	private static final String URI_PATH_PLAYLISTS = "me/playlists";

	private final WebClient webClient;
	private final JsonObject config;

	public GetPlaylistsHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerResponse response = event.response();
		final String apiRoot = config.getString(CONFIG_PROPERTY_SPOTIFY_API_ROOT);
		final String uri = String.join("/", apiRoot, URI_PATH_PLAYLISTS);
		final String userId = event.pathParam(USER_ID_PARAMETER);
		final AuthorizationToken token = event.get(userId);

		webClient.getAbs(uri)//
				.bearerTokenAuthentication(token.getAccessToken())//
				.send(ar -> {
					if (ar.succeeded())
					{
						final HttpResponse<Buffer> apiResponse = ar.result();
						if (apiResponse.statusCode() == OK.code())
						{
							final PlaylistsArray playlistsArray = apiResponse.bodyAsJson(PlaylistsArray.class);
							respondWithOk(response, playlistsArray.toString());
						}
						else
						{
							LOG.error("Error while getting playlists for user {} from Spotify. HTTP code {}. " +//
									"Details: {}", userId, apiResponse.statusCode(), apiResponse.bodyAsString());
							respondWithInternalError(response);
						}
					}
					else
					{
						LOG.error("Connection error while getting playlists for user {}: {}.", //
								userId, ar.cause());
						respondWithInternalError(response);
					}
				});
	}
}
