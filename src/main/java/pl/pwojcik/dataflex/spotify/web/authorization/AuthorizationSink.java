package pl.pwojcik.dataflex.spotify.web.authorization;

import static io.netty.handler.codec.http.HttpHeaderNames.ACCESS_CONTROL_ALLOW_ORIGIN;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

/**
 * Class responsible for informing UI whether user is already logged in or not.
 * It uses logic of {@link AuthorizationInterceptingHandler} and if it succeeded, this sink just returns HTTP 200.
 */
public class AuthorizationSink implements Handler<RoutingContext>
{
	@Override
	public void handle(final RoutingContext event)
	{
		event.response()//
				.setStatusCode(HttpResponseStatus.OK.code())//
				.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN.toString(), "*")//
				.end();
	}
}
