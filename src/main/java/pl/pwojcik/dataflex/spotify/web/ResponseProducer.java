package pl.pwojcik.dataflex.spotify.web;

import static io.netty.handler.codec.http.HttpHeaderNames.ACCESS_CONTROL_ALLOW_ORIGIN;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpHeaderNames.LOCATION;
import static io.netty.handler.codec.http.HttpHeaderValues.APPLICATION_JSON;
import static io.netty.handler.codec.http.HttpResponseStatus.CREATED;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpServerResponse;

/**
 * Class aggregating static methods for producing HTTP response for Vertx objects.
 */
public class ResponseProducer
{
	/**
	 * Creates response with code HTTP 200 and given JSON as payload.
	 *
	 * @param response Vertx response object
	 * @param jsonBody payload as String
	 */
	public static void respondWithOk(final HttpServerResponse response, final String jsonBody)
	{
		response.setStatusCode(OK.code())//
				.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN.toString(), "*")//
				.putHeader(CONTENT_TYPE.toString(), APPLICATION_JSON.toString())//
				.end(jsonBody);
	}

	/**
	 * Creates response with code HTTP 201 and given Location header value.
	 *
	 * @param response Vertx response object
	 * @param locationHeaderValue value of Location header
	 */
	public static void respondWithCreated(final HttpServerResponse response, final String locationHeaderValue)
	{
		response.setStatusCode(CREATED.code())//
				.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN.toString(), "*")//
				.putHeader(LOCATION.toString(), locationHeaderValue)//
				.end();
	}

	/**
	 * Creates response with code HTTP 204.
	 *
	 * @param response Vertx response object
	 */
	public static void respondWithNoContent(final HttpServerResponse response)
	{
		response.setStatusCode(HttpResponseStatus.NO_CONTENT.code())//
				.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN.toString(), "*")//
				.end();
	}

	/**
	 * Creates response with code HTTP 302 and given Location header value.
	 *
	 * @param response Vertx response object
	 * @param locationHeaderValue value of Location header
	 */
	public static void respondWithFound(final HttpServerResponse response, final String locationHeaderValue)
	{
		response.setStatusCode(HttpResponseStatus.FOUND.code())//
				.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN.toString(), "*")//
				.putHeader(HttpHeaderNames.LOCATION.toString(), locationHeaderValue)//
				.end();
	}

	/**
	 * Creates response with code HTTP 400 and given payload.
	 *
	 * @param response Vertx response object
	 * @param message payload
	 */
	public static void respondWithBadRequest(final HttpServerResponse response, final String message)
	{
		response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code())//
				.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN.toString(), "*")//
				.end(message);
	}

	/**
	 * Creates response with code HTTP 401.
	 *
	 * @param response Vertx response object
	 */
	public static void respondWithUnauthorized(final HttpServerResponse response)
	{
		response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code())//
				.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN.toString(), "*")//
				.end();
	}

	/**
	 * Creates response with code HTTP 404.
	 *
	 * @param response Vertx response object
	 */
	public static void respondWithNotFound(final HttpServerResponse response, final String message)
	{
		response.setStatusCode(HttpResponseStatus.NOT_FOUND.code())//
				.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN.toString(), "*")//
				.end(message);
	}

	/**
	 * Creates response with code HTTP 500.
	 *
	 * @param response Vertx response object
	 */
	public static void respondWithInternalError(final HttpServerResponse response)
	{
		response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code())//
				.putHeader(ACCESS_CONTROL_ALLOW_ORIGIN.toString(), "*")//
				.end();
	}
}
