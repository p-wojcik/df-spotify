package pl.pwojcik.dataflex.spotify.web.playlist;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithNoContent;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.PLAYLIST_ID_PARAMETER;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.CreatePlaylistRequest;
import pl.pwojcik.dataflex.spotify.model.Playlist;

/**
 * Handler responsible for changing user playlist's state from/to private/public.
 */
public class PublishPlaylistHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(PublishPlaylistHandler.class);
	private static final String CONFIG_PROPERTY_SPOTIFY_API_ROOT = "spotify.api.url";
	private static final String URI_PATH_PLAYLISTS = "playlists";
	private static final String QUERY_PARAM_FIELDS = "fields";
	private static final String FIELDS_TO_FETCH = "id,name,description,public,owner.id,owner.display_name";

	private final WebClient webClient;
	private final JsonObject config;

	public PublishPlaylistHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerRequest request = event.request();
		final HttpServerResponse response = event.response();
		final String playlistId = request.getParam(PLAYLIST_ID_PARAMETER);
		final String apiRoot = config.getString(CONFIG_PROPERTY_SPOTIFY_API_ROOT);
		final String uri = String.join("/", apiRoot, URI_PATH_PLAYLISTS, playlistId);
		final boolean promoteToPublic = request.method() == HttpMethod.PUT;
		final String userId = event.pathParam(USER_ID_PARAMETER);
		final AuthorizationToken token = event.get(userId);

		getPlaylistDetails(uri, userId, token).future()//
				.map(oldPlaylist -> {
					final CreatePlaylistRequest newPlaylist = new CreatePlaylistRequest(//
							oldPlaylist.getName(), oldPlaylist.getDescription(), promoteToPublic);
					LOG.debug("Updating playlist for user {}. Details: {}", userId, newPlaylist);
					return updatePlaylist(newPlaylist, uri, userId, token).future();
				})//
				.setHandler(result -> {
					if (result.succeeded())
					{
						respondWithNoContent(response);
					}
					else
					{
						respondWithInternalError(response);
					}
				});
	}

	private Promise<Playlist> getPlaylistDetails(final String uri, final String userId, final AuthorizationToken token)
	{
		final Promise<Playlist> playlistHolder = Promise.promise();
		webClient.getAbs(uri)//
				.bearerTokenAuthentication(token.getAccessToken())//
				.addQueryParam(QUERY_PARAM_FIELDS, FIELDS_TO_FETCH)//
				.send(ar -> {
					if (ar.succeeded())
					{
						final HttpResponse<Buffer> response = ar.result();
						if (response.statusCode() == OK.code())
						{
							final Playlist playlist = response.bodyAsJson(Playlist.class);
							playlistHolder.complete(playlist);
						}
						else
						{
							LOG.error("Failed to get playlist details before update for user {}. " +//
									"Received HTTP code {}. Details: {}", userId, response.statusCode(), response.bodyAsString());
							playlistHolder.fail(
									"Failed to get playlist details before update. Received HTTP " + response.statusCode());
						}
					}
					else
					{
						LOG.error("Failed to get playlist details before update for user {}. Connection error. Cause: {}",//
								userId, ar.cause());
						playlistHolder.fail("Failed to get playlist details before update. Connection error.");
					}
				});
		return playlistHolder;
	}

	private Promise<Void> updatePlaylist(final CreatePlaylistRequest createPlaylistRequest, final String uri,//
			final String userId, final AuthorizationToken token)
	{
		final Promise<Void> result = Promise.promise();
		final JsonObject playlistAsJson = JsonObject.mapFrom(createPlaylistRequest);
		webClient.putAbs(uri)//
				.bearerTokenAuthentication(token.getAccessToken())//
				.sendJsonObject(playlistAsJson, ar -> {
					if (ar.succeeded())
					{
						final HttpResponse<Buffer> response = ar.result();
						if (response.statusCode() == OK.code())
						{
							result.complete(null);
						}
						else
						{
							LOG.error("Failed to update playlist details for user {}. Received HTTP code {}. " +//
									"Details: {}", userId, response.statusCode(), response.bodyAsString());
							result.fail("Failed to update playlist details. Received HTTP " + response.statusCode());
						}
					}
					else
					{
						LOG.error("Failed to update playlist details for user {}. Connection error. Cause: {}",//
								userId, ar.cause());
						result.fail("Failed to update playlist details. Connection error.");
					}
				});
		return result;
	}
}
