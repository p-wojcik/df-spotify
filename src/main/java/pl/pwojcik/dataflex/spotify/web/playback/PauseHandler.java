package pl.pwojcik.dataflex.spotify.web.playback;

import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithNoContent;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpStatusClass;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;

/**
 * Handler responsible for starting/resuming playback on Spotify-compliant device.
 */
public class PauseHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(TransferPlaybackHandler.class);

	private static final String CONFIG_PROPERTY_SPOTIFY_API_ROOT = "spotify.api.url";
	private static final String SPOTIFY_PAUSE_PLAYER_URI = "me/player/pause";

	private final WebClient webClient;
	private final JsonObject config;

	public PauseHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerRequest request = event.request();
		final HttpServerResponse response = event.response();
		final String userId = request.getParam(USER_ID_PARAMETER);
		final String apiRoot = config.getString(CONFIG_PROPERTY_SPOTIFY_API_ROOT);
		final String uri = String.join("/", apiRoot, SPOTIFY_PAUSE_PLAYER_URI);
		final AuthorizationToken token = event.get(userId);

		LOG.debug("Pausing playback on for user {}", userId);
		webClient.putAbs(uri)//
				.bearerTokenAuthentication(token.getAccessToken())//
				.putHeader(HttpHeaderNames.CONTENT_LENGTH.toString(), "0")//
				.send(ar -> {
					if (ar.succeeded())
					{
						final HttpResponse<Buffer> apiResponse = ar.result();
						final int code = apiResponse.statusCode();
						// Forbidden means we're pausing track when nothing is playing.
						if (HttpStatusClass.valueOf(apiResponse.statusCode()) == HttpStatusClass.SUCCESS //
								|| code == HttpResponseStatus.FORBIDDEN.code())
						{
							respondWithNoContent(response);
						}
						else
						{
							LOG.error("Error while pausing playback for user {}. HTTP code: {}. Cause: {}",//
									userId, code, apiResponse.bodyAsString());
							respondWithInternalError(response);
						}
					}
					else
					{
						LOG.error("Connection error while pausing playback for user {}. Cause: {}", userId, ar.cause());
						respondWithInternalError(response);
					}
				});
	}
}
