package pl.pwojcik.dataflex.spotify.web;

import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithBadRequest;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import pl.pwojcik.dataflex.spotify.exception.ValidationException;

/**
 * Handler that maps thrown exceptions to HTTP responses.
 */
public class ExceptionHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(ExceptionHandler.class);

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerResponse response = event.response();
		final Throwable failure = event.failure();
		if (failure instanceof ValidationException)
		{
			respondWithBadRequest(response, failure.getMessage());
		}
		else
		{
			LOG.error("Unknown error occurred: ", failure);
			respondWithInternalError(response);
		}
	}
}
