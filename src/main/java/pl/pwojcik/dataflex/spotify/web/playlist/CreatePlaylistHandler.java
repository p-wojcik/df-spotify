package pl.pwojcik.dataflex.spotify.web.playlist;

import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithBadRequest;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithCreated;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.handler.codec.http.HttpStatusClass;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.CreatePlaylistRequest;
import pl.pwojcik.dataflex.spotify.model.Playlist;
import pl.pwojcik.dataflex.spotify.validator.CreatePlaylistRequestValidator;

/**
 * Handler responsible for creating new playlists in Spotify.
 */
public class CreatePlaylistHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(CreatePlaylistHandler.class);
	private static final String CONFIG_PROPERTY_SPOTIFY_API_ROOT = "spotify.api.url";
	private static final String URI_PATH_USERS = "users";
	private static final String URI_PATH_PLAYLISTS = "playlists";

	private final WebClient webClient;
	private final JsonObject config;

	public CreatePlaylistHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerRequest request = event.request();
		final HttpServerResponse response = event.response();
		final String userId = request.getParam(USER_ID_PARAMETER);
		final String apiRoot = config.getString(CONFIG_PROPERTY_SPOTIFY_API_ROOT);
		final String uri = String.join("/", apiRoot, URI_PATH_USERS, userId, URI_PATH_PLAYLISTS);
		final AuthorizationToken token = event.get(userId);

		try
		{
			final CreatePlaylistRequest createPlaylistRequest = Json.decodeValue(event.getBody(), CreatePlaylistRequest.class);
			CreatePlaylistRequestValidator.validate(createPlaylistRequest);
			final JsonObject playlistAsJson = JsonObject.mapFrom(createPlaylistRequest);
			LOG.debug("Storing new playlist for user {} : {}", userId, playlistAsJson.toString());
			webClient.postAbs(uri)//
					.bearerTokenAuthentication(token.getAccessToken())//
					.sendJsonObject(playlistAsJson, ar -> {
						if (ar.succeeded())
						{
							final HttpResponse<Buffer> creationResponse = ar.result();
							if (HttpStatusClass.valueOf(creationResponse.statusCode()) == HttpStatusClass.SUCCESS)
							{
								final Playlist createdPlaylist = creationResponse.bodyAsJson(Playlist.class);
								final String locationHeader = String.join("/", uri, createdPlaylist.getId());
								respondWithCreated(response, locationHeader);
							}
							else
							{
								LOG.error("Error while storing new playlist for user {}: {}. HTTP code: {}. Cause: {}", //
										userId, playlistAsJson.toString(), creationResponse.statusCode(), creationResponse.bodyAsString());
								respondWithInternalError(response);
							}
						}
						else
						{
							LOG.error("Connection error while storing new playlist for user {}: {}. Cause: {}", //
									userId, playlistAsJson.toString(), ar.cause());
							respondWithInternalError(response);
						}
					});
		}
		catch (final DecodeException e)
		{
			LOG.error("Error while decoding new playlist JSON for user {}: {}. Cause: {}", //
					userId, event.getBodyAsString(), e);
			respondWithBadRequest(response, "Given JSON is not valid playlist representation.");
		}
	}
}
