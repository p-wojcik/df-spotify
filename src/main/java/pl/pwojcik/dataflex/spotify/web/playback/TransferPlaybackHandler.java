package pl.pwojcik.dataflex.spotify.web.playback;

import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithNoContent;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithNotFound;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpStatusClass;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.validator.DeviceIdValidator;

/**
 * Handler responsible for transferring current playback to another device.
 * Should be used only when something is really played at the moment.
 */
public class TransferPlaybackHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(TransferPlaybackHandler.class);

	private static final String CONFIG_PROPERTY_SPOTIFY_API_ROOT = "spotify.api.url";
	private static final String SPOTIFY_PLAYER_URI = "me/player";
	private static final String QUERY_PARAM_DEVICE = "device";
	private static final String DEVICE_IDS_ATTRIBUTE = "device_ids";

	private final WebClient webClient;
	private final JsonObject config;

	public TransferPlaybackHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerRequest request = event.request();
		final HttpServerResponse response = event.response();
		final String userId = request.getParam(USER_ID_PARAMETER);
		final String apiRoot = config.getString(CONFIG_PROPERTY_SPOTIFY_API_ROOT);
		final String deviceId = request.getParam(QUERY_PARAM_DEVICE);
		final String uri = String.join("/", apiRoot, SPOTIFY_PLAYER_URI);
		final AuthorizationToken token = event.get(userId);

		if (DeviceIdValidator.isValid(deviceId))
		{
			LOG.debug("Transferring playback to device {} for user {}", deviceId, userId);
			final JsonObject payload = preparePayload(deviceId);
			webClient.putAbs(uri)//
					.bearerTokenAuthentication(token.getAccessToken())//
					.sendJsonObject(payload, ar -> {
						if (ar.succeeded())
						{
							final HttpResponse<Buffer> apiResponse = ar.result();
							if (HttpStatusClass.valueOf(apiResponse.statusCode()) == HttpStatusClass.SUCCESS)
							{
								respondWithNoContent(response);
							}
							else
							{
								LOG.error("Error while transferring playback to device for user {}: {}. HTTP code: {}. Cause: {}", //
										deviceId, userId, apiResponse.statusCode(), apiResponse.bodyAsString());
								respondWithInternalError(response);
							}
						}
						else
						{
							LOG.error("Connection error while transferring playback to device for user {}: {}. Cause: {}", //
									deviceId, userId, ar.cause());
							respondWithInternalError(response);
						}
					});
		}
		else
		{
			respondWithNotFound(response, "Device with given ID not found.");
		}
	}

	private JsonObject preparePayload(final String deviceId)
	{
		final JsonArray idsArray = new JsonArray(ImmutableList.of(deviceId));
		return new JsonObject(ImmutableMap.of(DEVICE_IDS_ATTRIBUTE, idsArray));
	}
}
