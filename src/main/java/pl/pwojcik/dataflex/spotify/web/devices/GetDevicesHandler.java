package pl.pwojcik.dataflex.spotify.web.devices;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithOk;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.Device;

/**
 * Handler responsible for getting list of devices that can be used to play Spotify tracks on.
 */
public class GetDevicesHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(GetDevicesHandler.class);
	private static final String CONFIG_PROPERTY_SPOTIFY_API_ROOT = "spotify.api.url";
	private static final String URI_PATH_DEVICES = "me/player/devices";
	private static final String DEVICES_JSON_ATTRIBUTE = "devices";

	private final WebClient webClient;
	private final JsonObject config;

	public GetDevicesHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerResponse response = event.response();
		final String apiRoot = config.getString(CONFIG_PROPERTY_SPOTIFY_API_ROOT);
		final String uri = String.join("/", apiRoot, URI_PATH_DEVICES);
		final String userId = event.pathParam(USER_ID_PARAMETER);
		final AuthorizationToken token = event.get(userId);

		webClient.getAbs(uri)//
				.bearerTokenAuthentication(token.getAccessToken())//
				.send(ar -> {
					if (ar.succeeded())
					{
						final HttpResponse<Buffer> apiResponse = ar.result();
						if (apiResponse.statusCode() == OK.code())
						{

							final JsonObject devicesAsJson = apiResponse.bodyAsJsonObject();
							final JsonArray devicesArray = devicesAsJson.getJsonArray(DEVICES_JSON_ATTRIBUTE);

							final List<Device> devices = devicesArray.stream()//
									.map(JsonObject::mapFrom)//
									.map(device -> device.mapTo(Device.class))//
									.collect(Collectors.toList());
							final JsonArray repackedDevices = new JsonArray(devices);

							respondWithOk(response, repackedDevices.toString());
						}
						else
						{
							LOG.error("Error while getting devices for user {} from Spotify. HTTP code {}. " +//
									"Details: {}", userId, apiResponse.statusCode(), apiResponse.bodyAsString());
							respondWithInternalError(response);
						}
					}
					else
					{
						LOG.error("Connection error while getting devices for user {}: {}.", //
								userId, ar.cause());
						respondWithInternalError(response);
					}
				});
	}
}
