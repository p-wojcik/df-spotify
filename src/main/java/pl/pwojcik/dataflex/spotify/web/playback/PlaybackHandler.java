package pl.pwojcik.dataflex.spotify.web.playback;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithInternalError;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithNoContent;
import static pl.pwojcik.dataflex.spotify.web.ResponseProducer.respondWithNotFound;
import static pl.pwojcik.dataflex.spotify.web.RoutesProvider.USER_ID_PARAMETER;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpStatusClass;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.Toolbox;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.IdType;
import pl.pwojcik.dataflex.spotify.model.PlaybackTarget;
import pl.pwojcik.dataflex.spotify.service.ShareableIdTranslator;
import pl.pwojcik.dataflex.spotify.validator.DeviceIdValidator;
import pl.pwojcik.dataflex.spotify.validator.PlaybackTargetValidator;

/**
 * Handler responsible for starting/resuming playback on Spotify-compliant device.
 */
public class PlaybackHandler implements Handler<RoutingContext>
{
	private static final Logger LOG = LogManager.getLogger(TransferPlaybackHandler.class);

	private static final String CONFIG_PROPERTY_SPOTIFY_API_ROOT = "spotify.api.url";
	private static final String SPOTIFY_START_PLAYER_URI = "me/player/play";
	private static final String IN_QUERY_PARAM_DEVICE = "device";
	private static final String OUT_QUERY_PARAM_DEVICE_ID = "device_id";
	private static final String PAYLOAD_ATTRIBUTE_CONTEXT_URI = "context_uri";
	private static final String PAYLOAD_ATTRIBUTE_URIS = "uris";

	private final WebClient webClient;
	private final JsonObject config;
	private final ShareableIdTranslator translator;

	public PlaybackHandler(final Toolbox toolbox)
	{
		this.webClient = toolbox.getWebClient();
		this.config = toolbox.getConfig();
		this.translator = new ShareableIdTranslator();
	}

	@Override
	public void handle(final RoutingContext event)
	{
		final HttpServerRequest request = event.request();
		final HttpServerResponse response = event.response();
		final String userId = request.getParam(USER_ID_PARAMETER);
		final String apiRoot = config.getString(CONFIG_PROPERTY_SPOTIFY_API_ROOT);
		final String deviceId = event.queryParams().get(IN_QUERY_PARAM_DEVICE);
		final String uri = String.join("/", apiRoot, SPOTIFY_START_PLAYER_URI);
		final AuthorizationToken token = event.get(userId);

		if (DeviceIdValidator.isValid(deviceId))
		{
			final PlaybackTarget target = Json.decodeValue(event.getBody(), PlaybackTarget.class);
			PlaybackTargetValidator.validate(target);
			final String directSpotifyID = provideDirectSpotifyID(target);
			LOG.debug("Starting playback on device {} for user {} with target {}", deviceId, userId, target);

			final JsonObject payload = preparePayload(target.getTarget(), directSpotifyID);
			final HttpRequest<Buffer> apiRequest = webClient.putAbs(uri).bearerTokenAuthentication(token.getAccessToken());
			if (isNotBlank(deviceId))
			{
				apiRequest.addQueryParam(OUT_QUERY_PARAM_DEVICE_ID, deviceId);
			}
			apiRequest.sendJsonObject(payload, ar -> {
				if (ar.succeeded())
				{

					final HttpResponse<Buffer> apiResponse = ar.result();
					if (HttpStatusClass.valueOf(apiResponse.statusCode()) == HttpStatusClass.SUCCESS)
					{
						respondWithNoContent(response);
					}
					else
					{
						LOG.error("Error while starting playback on device {} for user {} with target {}. " +//
										"HTTP code: {}. Cause: {}", deviceId, userId, target, apiResponse.statusCode(),
								apiResponse.bodyAsString());
						respondWithInternalError(response);
					}
				}
				else
				{
					LOG.error("Connection error while starting playback on device {} for user {} " +//
							"with target {}. Cause: {}", deviceId, userId, target, ar.cause());
					respondWithInternalError(response);
				}
			});
		}
		else
		{
			respondWithNotFound(response, "Device with given ID not found.");
		}
	}

	private String provideDirectSpotifyID(final PlaybackTarget target)
	{
		if (target.getType() == IdType.SHAREABLE)
		{
			return translator.translateToDirectID(target);
		}
		return target.getId();
	}

	private JsonObject preparePayload(final PlaybackTarget.Target playbackTarget, final String directID)
	{
		if (playbackTarget == PlaybackTarget.Target.CONTEXT)
		{
			return new JsonObject(ImmutableMap.of(PAYLOAD_ATTRIBUTE_CONTEXT_URI, directID));
		}

		final JsonArray urisArray = new JsonArray(ImmutableList.of(directID));
		return new JsonObject(ImmutableMap.of(PAYLOAD_ATTRIBUTE_URIS, urisArray));
	}
}
