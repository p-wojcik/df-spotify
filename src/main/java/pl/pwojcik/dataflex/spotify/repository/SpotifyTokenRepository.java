package pl.pwojcik.dataflex.spotify.repository;

import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;
import pl.pwojcik.dataflex.spotify.exception.NoAccessTokenCached;
import pl.pwojcik.dataflex.spotify.model.AuthorizationToken;
import pl.pwojcik.dataflex.spotify.model.SpotifyUserId;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Redis repository of OAuth2 tokens used for communication with Spotify API.
 * Access tokens are stored in cache to avoid forcing Authorization Code flow for each user's request.
 */
public class SpotifyTokenRepository
{
	private static final Logger LOG = LogManager.getLogger(SpotifyTokenRepository.class);
	private static final String USER_KEY_PREFIX = "spotifyUserId:";
	private static final String ACCOUNT_TYPE_ATTRIBUTE = "accountType";

	private RedisClient redisClient;

	public SpotifyTokenRepository(final Vertx vertx)
	{
		redisClient = RedisClient.create(vertx, new RedisOptions());
	}

	/**
	 * Saves token for user in cache.
	 *
	 * @param userId spotify ID of user
	 * @param token obtained access token
	 * @return true if caching succeeded
	 */
	public Promise<Boolean> storeTokenForUser(final SpotifyUserId userId, final AuthorizationToken token)
	{
		final Promise<Boolean> promise = Promise.promise();
		final String key = USER_KEY_PREFIX + userId.getId();
		final JsonObject valueAsJson = JsonObject.mapFrom(token);
		valueAsJson.put(ACCOUNT_TYPE_ATTRIBUTE, userId.getAccountType());
		LOG.debug("Storing access token in Redis user {}", userId);
		redisClient.hmset(key, valueAsJson, ar -> {
			if (ar.failed())
			{
				LOG.error("Storing access token in Redis for user {} failed. Cause: {}", userId.toString(), ar.cause());
				promise.fail(ar.cause());
			}
			else
			{
				LOG.debug("Access token for user {} stored in Redis successfully", userId);
				promise.complete(ar.succeeded());
			}
		});
		return promise;
	}

	/**
	 * Removes token for user.
	 *
	 * @param userId spotify ID of user
	 * @return true if deletion succeeded
	 */
	public Promise<Boolean> deleteTokenForUser(final SpotifyUserId userId)
	{
		final Promise<Boolean> promise = Promise.promise();
		final String key = USER_KEY_PREFIX + userId.getId();
		LOG.debug("Removing access token from Redis for user {}", userId);
		redisClient.del(key, ar -> {
			if (ar.failed())
			{
				LOG.error("Removing access token from Redis for user {} failed. Cause: {}", userId.toString(), ar.cause());
				promise.fail(ar.cause());
			}
			else
			{
				LOG.debug("Access token for user {} removed from Redis successfully", userId);
				promise.complete(ar.succeeded());
			}
		});
		return promise;
	}

	/**
	 * Gets token for user from cache
	 *
	 * @param userId spotify ID of user
	 * @return related access token
	 */
	public Promise<Pair<String, AuthorizationToken>> getTokenForUser(final SpotifyUserId userId)
	{
		final Promise<Pair<String, AuthorizationToken>> promise = Promise.promise();
		final String key = USER_KEY_PREFIX + userId.getId();
		redisClient.hgetall(key, ar -> {
			if (ar.succeeded())
			{
				final JsonObject tokenAsJson = ar.result();
				if (tokenAsJson.isEmpty())
				{
					LOG.debug("No access token found for user {}", userId);
					promise.fail(new NoAccessTokenCached(userId.getId()));
				}
				else
				{
					LOG.debug("Retrieved access token from Redis for user {}", userId);
					final String accountType = (String) tokenAsJson.remove(ACCOUNT_TYPE_ATTRIBUTE);
					final AuthorizationToken token = tokenAsJson.mapTo(AuthorizationToken.class);
					promise.complete(Pair.of(accountType, token));
				}
			}
			else
			{
				promise.fail(ar.cause());
			}
		});
		return promise;
	}
}
