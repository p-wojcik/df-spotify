package pl.pwojcik.dataflex.spotify;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.repository.SpotifyTokenRepository;
import pl.pwojcik.dataflex.spotify.service.UserIdFetchingService;
import pl.pwojcik.dataflex.spotify.web.RoutesProvider;

/**
 * Entry point of the application.
 */
public class ApplicationRunner extends AbstractVerticle
{
	private static final Logger LOG = LogManager.getLogger(ApplicationRunner.class);
	private static final String PROPERTY_KEY_APPLICATION_PORT = "application.port";

	private static final String PROPERTIES_FILENAME = "default.properties";

	@Override
	public void start(Promise<Void> startPromise)
	{
		LOG.info("Starting an application...");

		final Toolbox.Builder toolboxBuilder = new Toolbox.Builder();
		final Router router = Router.router(vertx);
		final WebClient webClient = WebClient.create(vertx);
		toolboxBuilder.webClient(webClient);
		final ConfigRetriever retriever = createConfigRetriever();

		retriever.getConfig(result -> {
			if (result.succeeded())
			{
				LOG.debug("Vertx Config initialized.");
				final JsonObject config = result.result();
				toolboxBuilder.configuration(config);
				toolboxBuilder.spotifyTokenRepository(new SpotifyTokenRepository(vertx));
				toolboxBuilder.userIdFetchingService(new UserIdFetchingService(webClient, config));
				final Toolbox toolbox = toolboxBuilder.build();
				RoutesProvider.bindRoutes(router, toolbox, vertx);

				final int port = config.getInteger(PROPERTY_KEY_APPLICATION_PORT);

				vertx.createHttpServer()//
						.requestHandler(router)//
						.listen(port, http -> {
							if (http.succeeded())
							{
								LOG.info("Application started on port {}", port);
								startPromise.complete();
							}
							else
							{
								LOG.error("Application failed to start on port {}", port, http.cause());
								startPromise.fail(http.cause());
							}
						});
			}
			else
			{
				LOG.error("Failed to load properties file.");
				throw new IllegalStateException("Failed to load properties file.");
			}
		});
	}

	private ConfigRetriever createConfigRetriever()
	{
		final ConfigStoreOptions fileStore = new ConfigStoreOptions()//
				.setType("file")//
				.setFormat("properties")//
				.setConfig(new JsonObject().put("path", PROPERTIES_FILENAME));

		final ConfigRetrieverOptions retrieverOptions = new ConfigRetrieverOptions()//
				.addStore(fileStore);
		return ConfigRetriever.create(vertx, retrieverOptions);
	}
}
