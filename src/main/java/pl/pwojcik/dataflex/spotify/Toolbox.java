package pl.pwojcik.dataflex.spotify;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.nonNull;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import pl.pwojcik.dataflex.spotify.repository.SpotifyTokenRepository;
import pl.pwojcik.dataflex.spotify.service.UserIdFetchingService;

/**
 * Class aggregating clients & repositories used across differents part of this module.
 */
public class Toolbox
{
	private final WebClient webClient;
	private final SpotifyTokenRepository spotifyTokenRepository;
	private final UserIdFetchingService userIdFetchingService;
	private final JsonObject config;

	private Toolbox(final WebClient webClient, final SpotifyTokenRepository spotifyTokenRepository,
			UserIdFetchingService userIdFetchingService, final JsonObject config)
	{
		this.webClient = webClient;
		this.spotifyTokenRepository = spotifyTokenRepository;
		this.userIdFetchingService = userIdFetchingService;
		this.config = config;
	}

	public JsonObject getConfig()
	{
		return config;
	}

	public SpotifyTokenRepository getSpotifyTokenRepository()
	{
		return spotifyTokenRepository;
	}

	public UserIdFetchingService getUserIdFetchingService()
	{
		return userIdFetchingService;
	}

	public WebClient getWebClient()
	{
		return webClient;
	}

	/**
	 * Builder for {@linkplain Toolbox} class.
	 */
	static class Builder
	{
		private WebClient webClient;
		private SpotifyTokenRepository spotifyTokenRepository;
		private UserIdFetchingService userIdFetchingService;
		private JsonObject config;

		public Builder webClient(final WebClient webClient)
		{
			this.webClient = webClient;
			return this;
		}

		public Builder spotifyTokenRepository(final SpotifyTokenRepository spotifyTokenRepository)
		{
			this.spotifyTokenRepository = spotifyTokenRepository;
			return this;
		}

		public Builder userIdFetchingService(final UserIdFetchingService userIdFetchingService) {
			this.userIdFetchingService = userIdFetchingService;
			return this;
		}

		public Builder configuration(final JsonObject config)
		{
			this.config = config;
			return this;
		}

		public Toolbox build()
		{
			checkArgument(nonNull(webClient), "Web Client cannot be null.");
			checkArgument(nonNull(spotifyTokenRepository), "Spotify Token repository cannot be null");
			checkArgument(nonNull(config), "Configuration JSON cannot be null");
			checkArgument(nonNull(userIdFetchingService), "User ID fetching service cannot be null");

			return new Toolbox(webClient, spotifyTokenRepository, userIdFetchingService, config);
		}
	}
}
