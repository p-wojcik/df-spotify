package pl.pwojcik.dataflex.spotify.exception;

/**
 * Exception indicating that there's no cached access token for given user.
 */
public class NoAccessTokenCached extends RuntimeException
{
	public NoAccessTokenCached(final String userName)
	{
		super("No access token available for user " + userName);
	}
}
