package pl.pwojcik.dataflex.spotify.exception;

/**
 * Exception indicating that data does not meet business requirements somehow.
 */
public class ValidationException extends RuntimeException
{
	public ValidationException(final String msg)
	{
		super(msg);
	}
}
